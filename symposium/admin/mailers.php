<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require '../vendor/autoload.php';
require_once "../config.php";

// Check if email already exists
$sql = "SELECT * FROM tbl_users WHERE emailid='$email'";
$result = mysqli_query($link, $sql);
if (mysqli_affected_rows($link) > 0) 
{
    //send email
    $mail = new PHPMailer(true);
    
    try {
        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'mail.genseer.com';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'webinars@genseer.com';                     // SMTP username
        $mail->Password   = 'GensWebinars@#$1234';                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS; //STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
    
        //Recipients
        $mail->setFrom('webinars@genseer.com', 'Genseer Webinars');
        $mail->addAddress($email, $fname);     // Add a recipient
    
        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Verify your Email-Id for Online Webinar';
        
        $message = "Hello ". $fname . ",";
        $message .="<br><br>";
        $message .="You are now registered for Genseer Webinars.";
        $message .="<br>Please Verify your email address by clicking on following link: <a href='https://webinars.genseer.com/verify.php?e=".$email."&t=".$token."' target='_blank'>Verify your email address</a>";
        $message .="<br><br>";
        $message .="Thanks,<br>Team Genseer Webinars";
        
        $mail->Body    = $message;
    
        $mail->send();
        
    } 
    catch (Exception $e) {
        $errors["mail"] = "Mail could not be sent. Please try again";
        $errors['mailerror'] =  'Mailer Error: ' . $mail->ErrorInfo;
    }
    
    $succ = 'You are successfully registered! Please verify your email address. You will be redirected to login page in 5 seconds.';
    //header('location: index.php');
} 
else 
{
    $_SESSION['error_msg'] = "Database error: Could not get users";
}

?>