<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row content">
        <div class="col-10 col-md-6 col-lg-4 offset-md-3 offset-lg-4 offset-1">
            <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                  </div>
                  
                  <div class="input-group">
                    <input type="email" class="form-control" placeholder="Email ID" aria-label="Email ID" aria-describedby="basic-addon1" name="emailid" id="emailid" required>
                  </div>
                  <div class="input-group">
                    <button id="login" class="btn login-button" type="submit">Login</button>
                  </div>
                  
                  <div class="input-group info-text">
                    If not registered, click <a href="register.php">here</a>
                  </div>
                
            </form>        
        </div>
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
  $.post('chkforlogin.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
        window.location.href='webcast.php';  
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already logged in. Please logout and try again.');
          $('#login-message').addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
          return false;
      }
  });
  
  return false;
});
</script>
</body>
</html>