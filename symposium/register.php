<!doctype html>
<html>
<head>
<meta charset="utf-8">
<link rel="icon" href="img/favicon.png" type="image/png">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Live Webcast</title>
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>
<div class="container-fluid">
    <div class="row content">
        <div class="col-10 col-md-8 col-lg-6 offset-md-7 offset-lg-5 offset-1">
            <form id="login-form" method="post" role="form">
                  <div id="login-message"></div>
                  <div class="row">
                      <div class="input-group col-12 col-md-6">
                        <input type="text" class="form-control" placeholder="Name" aria-label="Name" aria-describedby="basic-addon1" name="name" id="name" required>
                      </div>
                      <div class="input-group col-12 col-md-6">
                        <input type="text" class="form-control" placeholder="Academic Title" aria-label="Academic Title" aria-describedby="basic-addon1" name="title" id="title" required>
                      </div>
                      <div class="input-group col-12 col-md-6">
                        <input type="text" class="form-control" placeholder="Specialty" aria-label="Specialty" aria-describedby="basic-addon1" name="specialty" id="specialty" required>
                      </div>
                      <div class="input-group col-12 col-md-6">
                        <input type="text" class="form-control" placeholder="Country" aria-label="Country" aria-describedby="basic-addon1" name="country" id="country" required>
                      </div>
                      <div class="input-group col-12 col-md-6">
                        <input type="text" class="form-control" placeholder="City" aria-label="City" aria-describedby="basic-addon1" name="city" id="city" required>
                      </div>
                      <div class="input-group col-12 col-md-6">
                        <input type="text" class="form-control" placeholder="Hospital" aria-label="Hospital" aria-describedby="basic-addon1" name="hospital" id="hospital" required>
                      </div>
                  <div class="input-group col-12 col-md-6">
                    <input type="email" class="form-control" placeholder="Email ID" aria-label="Email ID" aria-describedby="basic-addon1" name="emailid" id="emailid" required>
                  </div>
                  <div class="input-group col-12 col-md-6">
                    <input type="number" class="form-control" placeholder="Mobile No." aria-label="Mobile No." aria-describedby="basic-addon1" name="mobile" id="mobile" required>
                  </div>
                  </div>
                  
                  <div class="input-group">
                    <button id="register" class="btn login-button" type="submit">Register</button>
                  </div>
                  
                  <div class="input-group info-text">
                    If already registered, click <a href="index.php">here</a>
                  </div>
				 
                
            </form>
					<div class="mob">
						<img src='img/text-bot.png' width="280px">
					</div>	
        </div>
    </div>
    
</div>
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script>
$(document).on('submit', '#login-form', function()
{  
    $('#register').attr('disabled','disabled');
  $.post('chkforreg.php', $(this).serialize(), function(data)
  {
     // console.log(data);
      if(data == 's')
      {
          $("#login-form").trigger("reset");
        $('#login-message').text('You are registered successfully.');
        $('#login-message').removeClass().addClass('alert alert-success').fadeIn().delay(3000).fadeOut();
      $('#register').prop('disabled',false);
        return false; 
      }
      else if (data == '-1')
      {
          $('#login-message').text('You are already registered.');
          $('#login-message').removeClass().addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
      $('#register').prop('disabled',false);
          return false;
      }
      else
      {
          $('#login-message').text(data);
          $('#login-message').addClass('alert alert-danger').fadeIn().delay(3000).fadeOut();
      $('#register').prop('disabled',false);
          return false;
      }
      
  });
  
  return false;
});
</script>
</body>
</html>