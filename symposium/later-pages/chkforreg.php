<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';
require_once "config.php";

$user_emailid     = mysqli_real_escape_string($link, $_POST['emailid']);
$user_name     = mysqli_real_escape_string($link, $_POST['name']);
$user_title     = mysqli_real_escape_string($link, $_POST['title']);
$user_specialty     = mysqli_real_escape_string($link, $_POST['specialty']);
$user_country     = mysqli_real_escape_string($link, $_POST['country']);
$user_city     = mysqli_real_escape_string($link, $_POST['city']);
$user_hospital     = mysqli_real_escape_string($link, $_POST['hospital']);
$user_mobile     = mysqli_real_escape_string($link, $_POST['mobile']);

$query="select * from tbl_users where user_emailid ='$user_emailid' and eventname='$event_name'";
$res = mysqli_query($link, $query) or die(mysqli_error($link)); 

if (mysqli_affected_rows($link) > 0) 
{
    echo "-1";
    
}
else{
    //echo "0";
    $reg_date   = date('Y/m/d H:i:s');

    $query="insert into tbl_users(user_name, user_title, user_specialty, user_country, user_city, user_emailid, user_hospital, user_mobile, joining_date, logout_status, eventname) values('$user_name','$user_title','$user_specialty','$user_country','$user_city','$user_emailid','$user_hospital','$user_mobile','$reg_date','0','$event_name')";
    //echo $query;
    $res = mysqli_query($link, $query) or die(mysqli_error($link));
    
    $reg_id = mysqli_insert_id($link);
    if($reg_id > 0 )
    {
        $mail = new PHPMailer(true);

        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'support@coact.co.in';                     // SMTP username
        $mail->Password   = 'coact2020';                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
    
        //Recipients
        $mail->setFrom('support@coact.co.in', 'VitalMedCom');
        $mail->addAddress($user_emailid, $user_name);     // Add a recipient
    
        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Thank you for your registration!';
        
        $message = "
            Dear doctor, <br>
<br>
            Thank you for registering for the General Practitioner Academy webinar. Here are the event details:<br>
<br>            
            Date: 8 May 2020 <br>
            Time: 1PM-3PM (UAE time) <br>
            Link to join: https://coact.live/vitalmedcom/ <br>
<br>            
            The event is organized by Vital MedCom and supported by an unrestricted educational grant from Menarini.<br>
<br>            
            We look forward to your participation!";
        
        $mail->Body    = $message;
    
        $mail->send();

        
        echo "s";
    }
    else{
        echo "Please try again.";
    }
}
        

?>