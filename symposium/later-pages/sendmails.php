<?php
set_time_limit(100);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';
require_once "config.php";

$start_date   = date('Y/m/d H:i:s');
echo "Started at: ".$start_date ."<br><br>";

// INIT + RUN SETTINGS
set_time_limit(0);
$each = 4;
$pause = 2;
$mail_msg = '';

$sql = "SELECT COUNT(*) `cnt` FROM `tbl_users`";
$res = mysqli_query($link, $sql);

$data = mysqli_fetch_assoc($res);
 
// NOW TO SEND THE EMAIL - BATCH BY BATCH
$all = $data['cnt'];
$mail_sent = 0;
$mail_notsent = 0;

for ($i=150; $i<$all; $i+=$each) {
  $sql = "SELECT * FROM `tbl_users` LIMIT $i, $each";
  $res = mysqli_query($link, $sql);
  
  while($data = mysqli_fetch_assoc($res))
  {
    $name = $data['user_name'];
    $email = $data['user_emailid'];
    //echo $data['user_emailid'].'<br>'; 
    $mail = new PHPMailer(true);
    
    try {
        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
        $mail->isSMTP();                                            // Send using SMTP
        $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'support@coact.co.in';                     // SMTP username
        $mail->Password   = 'coact2020';                               // SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
        $mail->Port       = 465;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
    
        //Recipients
        $mail->setFrom('support@coact.co.in', 'VitalMedCom');
        $mail->addAddress($email, $name);     // Add a recipient
    
        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Reminder: General Practitioner Academy webinar';
        
        $message = "
        Dear doctor, <br>
<br>
            Thank you for registering for the General Practitioner Academy webinar. Here are the event details:<br>
<br>            
            Date: 8 May 2020 <br>
            Time: 1PM-3PM (UAE time) <br>
            Link to join: https://coact.live/vitalmedcom/ <br>
<br>            
            The event is organized by Vital MedCom and supported by an unrestricted educational grant from Menarini.<br>
<br>            
            We look forward to your participation! ";
        
        $mail->Body    = $message;
    
        if($mail->send())
        {
            $mail_sent++;
            $mail_msg .= "<br>Mail sent to " . $data['user_emailid'];
        }
        else{
            $mail_notsent++;
            $mail_msg .= "<br>Mail could not be sent to " . $data['user_emailid'];
        }
        //$mail->send();
        //$mail_msg .= "<br>" . $data['user_emailid'];

    } 
    catch (Exception $e) {
        //$mail_msg .= "Mail could not be sent to " . $data['user_emailid'];
        //$errors['mailerror'] =  'Mailer Error: ' . $mail->ErrorInfo;
    }  
  }
//  echo $sql.'<br>';
/*  foreach ($subscribers as $sub) {
    $msg = str_replace("[NAME]", $sub['name'], $template);
    $news->send($sub['email'], $msg);
  }
  */
  sleep($pause);
}

echo $mail_msg.'<br>';

echo "Mail Sent to: " . $mail_sent ."<br>";
echo "Mail not Sent to: " . $mail_notsent ."<br>";
echo "Total Members: " . $all."<br><br>";

$end_date   = date('Y/m/d H:i:s');
echo "Finished at: ".$end_date ."<br><br>";

?>